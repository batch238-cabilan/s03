-- Inserting Records
INSERT INTO artists (name) VALUES("Rivermaya");
INSERT INTO artists (name) VALUES("Psy");

INSERT INTO albums (album_title, data_released, artist_id) VALUES(
	"Psy 6",
	"2012-1-1",
	2
);

INSERT INTO albums (album_title, data_released, artist_id) VALUES(
	"Trip",
	"1996-1-1",
	1
);


INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Gangnam Style",
	253,
	"K-pop",
	1
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Kundiman",
	234,
	"OPM",
	2
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Kisapmata",
	259,
	"OPM",
	2
);

INSERT INTO users (email, password) VALUES(
	"user1@mail.com",
	"password"
);

-- Selecting Records
-- Display all songs

SELECT * FROM songs;

-- Display the title and genre of all songs
SELECT song_name, genre FROM songs;

-- Display the title of all OPM songs
SELECT song_name FROM songs WHERE genre = "OPM";

-- Display the song name and length of OPM songs that are longer than 240 seconds

select song_name, length from songs where length > 240 and genre = "OPM";

-- Updating records
-- Updating the length of Kundiman to 2:40

UPDATE songs SET length = 240 WHERE song_name = "Kundiman";

-- Removing the WHERE clause will update ALL rows

-- Deleting records
-- Delete all OPM songs lomger than 2:40
DELETE FROM songs WHERE genre = "OPM" AND length > 240;

-- Removing the WHERE clause will delete ALL rows


